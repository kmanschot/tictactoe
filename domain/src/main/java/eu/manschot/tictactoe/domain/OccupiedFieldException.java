package eu.manschot.tictactoe.domain;

public class OccupiedFieldException extends Exception {
    public OccupiedFieldException(Position position) {
        super("The field is already occupied for position: " + position);
    }
}
