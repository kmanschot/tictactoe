package eu.manschot.tictactoe.domain;

public class FixedOptionsCheckWinStrategy implements CheckWinStrategy {
    @Override
    public boolean isWin(Board board, Position position) {
        Field[][] fields = board.getFields();

        return hasMatch(fields, pos(0, 0), pos(0, 1), pos(0, 2)) ||
        hasMatch(fields, pos(1, 0), pos(1, 1), pos(1, 2)) ||
        hasMatch(fields, pos(2, 0), pos(2, 1), pos(2, 2)) ||

        hasMatch(fields, pos(0, 0), pos(1, 0), pos(2, 0)) ||
        hasMatch(fields, pos(0, 1), pos(1, 1), pos(2, 1)) ||
        hasMatch(fields, pos(0, 2), pos(1, 2), pos(2, 2)) ||

        hasMatch(fields, pos(0, 0), pos(1, 1), pos(2, 2)) ||
        hasMatch(fields, pos(0, 2), pos(1, 1), pos(2, 0));
    }

    private static boolean hasMatch(Field[][] fields, Position pos0, Position pos1, Position pos2) {
        Field field0 = getField(fields, pos0);
        Field field1 = getField(fields, pos1);
        Field field2 = getField(fields, pos2);

        return field0.hasMark() && equalMarks(field0, field1) && equalMarks(field1, field2);
    }

    private static Position pos(int column, int row) {
        return new Position(column, row);
    }

    private static boolean equalMarks(Field left, Field right) {
        return left.getMark().equals(right.getMark());
    }

    private static Field getField(Field[][] fields, Position position) {
        return fields[position.getColumn()][position.getRow()];
    }
}
