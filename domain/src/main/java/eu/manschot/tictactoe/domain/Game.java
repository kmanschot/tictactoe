package eu.manschot.tictactoe.domain;

import java.util.List;

import static eu.manschot.tictactoe.domain.GameState.*;

public class Game {
    private final List<Player> players;
    private final Board board;
    private final CheckWinStrategy checkWinStrategy;

    private GameState gameState;
    private Player currentPlayer;

    private Game(List<Player> players, Player currentPlayer, CheckWinStrategy checkWinStrategy) {
        this.players = players;
        this.currentPlayer = currentPlayer;
        this.checkWinStrategy = checkWinStrategy;

        board = new Board();

        gameState = GameState.IN_PROGRESS;
    }

    public Board getBoard() {
        return board;
    }

    public GameState getState() {
        return gameState;
    }

    public void markField(Position position) throws OccupiedFieldException {
        Mark mark = currentPlayer.getMark();

        board.markField(mark, position);

        boolean hasWon = checkWinStrategy.isWin(board, position);

        if (hasWon) {
            currentPlayer.addWin();
            gameState =  WIN;
        } else if (hasFullBoard()) {
            gameState = DRAW;
        } else {
            currentPlayer = GameUtil.getOpponent(players, currentPlayer);
        }
    }

    private boolean hasFullBoard() {
        Field[][] fields = board.getFields();
        for (Field[] column : fields) {
            for (Field field : column) {
                if (!field.hasMark()) {
                    return false;
                }
            }
        }
        return true;
    }

    public Player getCurrentPlayer() {
        return currentPlayer;
    }

    static Game start(List<Player> players, Player currentPlayer, CheckWinStrategy checkWinStrategy) {
        return new Game(players, currentPlayer, checkWinStrategy);
    }

    void end() {
        if (IN_PROGRESS.equals(gameState)) {
            gameState = DRAW;
        }
    }
}
