package eu.manschot.tictactoe.domain;

public class Field {
    private Mark mark;

    Field() {
        mark = null;
    }

    public boolean hasMark() {
        return mark != null;
    }

    public Mark getMark() {
        return mark;
    }

    void setMark(Mark mark) {
        this.mark = mark;
    }
}
