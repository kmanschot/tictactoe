package eu.manschot.tictactoe.domain;

public class RecursiveCheckWinStrategy implements CheckWinStrategy {

    @Override
    public boolean isWin(Board board, Position position) {
        Field[][] fields = board.getFields();

        return hasMatch(fields, position, 1, 0, 0, Direction.STRAIGHT) ||
                hasMatch(fields, position, 0, 1, 0, Direction.STRAIGHT) ||
                hasMatch(fields, position, 1, 1, 0, Direction.DIAGONAL) ||
                hasMatch(fields, position, -1, 1, 0, Direction.DIAGONAL);
    }

    private static boolean hasMatch(Field[][] fields, Position leftPos, int deltaX, int deltaY, int searchDepth, Direction direction) {
        int x = leftPos.getColumn() + deltaX;
        int y = leftPos.getRow() + deltaY;

        if (Direction.DIAGONAL.equals(direction) && !isCornerField(x, y)) {
            return false;
        }

        Position rightPos = createNextPositionInLoop(x, y);

        if (searchDepth == Board.NUMBER_OF_COLUMNS || rightPos.getRow() == Board.NUMBER_OF_ROWS) {
            return true;
        }

        Field left = getField(fields, leftPos);
        Field right = getField(fields, rightPos);

        return left.hasMark() && equalMarks(left, right) && hasMatch(fields, rightPos, deltaX, deltaY, ++searchDepth, direction);
    }

    private static Position createNextPositionInLoop(int x, int y) {
        int newX = redirectCoordinate(x);
        int newY = redirectCoordinate(y);

        return new Position(newX, newY);
    }

    private static int redirectCoordinate(int coordinate) {
        coordinate = coordinate < 0 ? coordinate + 3 : coordinate;
        return coordinate % 3;
    }

    private static boolean equalMarks(Field left, Field right) {
        return left.getMark().equals(right.getMark());
    }

    private static Field getField(Field[][] fields, Position position) {
        return fields[position.getColumn()][position.getRow()];
    }

    private static boolean isCornerField(int x, int y) {
        return x >= 0 && x <= 2 && y >= 0 && y <= 2 || ((x < 0 || x > 2) && (y < 0 || y > 2));
    }

    enum Direction {
        STRAIGHT,
        DIAGONAL
    }
}
