package eu.manschot.tictactoe.domain;

public class PartiallyRecursiveCheckWinStrategy implements CheckWinStrategy {

    @Override
    public boolean isWin(Board board, Position position) {
        Field[][] fields = board.getFields();

        return hasMatch(fields, new Position(0, 0), 0, 1) ||
                hasMatch(fields, new Position(1, 0), 0, 1) ||
                hasMatch(fields, new Position(2, 0), 0, 1) ||
                hasMatch(fields, new Position(0, 0), 1, 0) ||
                hasMatch(fields, new Position(0, 1), 1, 0) ||
                hasMatch(fields, new Position(0, 2), 1, 0) ||
                hasMatch(fields, new Position(0, 0), 1, 1) ||
                hasMatch(fields, new Position(0, 2), 1, -1);
    }

    private static boolean hasMatch(Field[][] fields, Position leftPos, int deltaX, int deltaY) {
        Position rightPos = new Position(leftPos.getColumn() + deltaX, leftPos.getRow() + deltaY);

        if(rightPos.getColumn() == Board.NUMBER_OF_COLUMNS || rightPos.getRow() == Board.NUMBER_OF_ROWS) {
            return true;
        }

        Field left = getField(fields, leftPos);
        Field right = getField(fields, rightPos);

        return left.hasMark() && equalMarks(left, right) && hasMatch(fields, rightPos, deltaX, deltaY);
    }

    private static boolean equalMarks(Field left, Field right) {
        return left.getMark().equals(right.getMark());
    }

    private static Field getField(Field[][] fields, Position position) {
        return fields[position.getColumn()][position.getRow()];
    }
}
