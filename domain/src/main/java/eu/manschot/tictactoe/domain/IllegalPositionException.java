package eu.manschot.tictactoe.domain;

public class IllegalPositionException extends Exception {
    public IllegalPositionException(String illegalPosition) {
        super("Illegal position: " + illegalPosition);
    }
}
