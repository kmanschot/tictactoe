package eu.manschot.tictactoe.domain;

public class Player {
    private final String name;

    private int score;
    private Mark mark;

    public Player(String name, Mark mark) {
        this.name = name;
        this.mark = mark;
        this.score = 0;
    }

    public Mark getMark() {
        return mark;
    }

    public String getName() {
        return name;
    }

    public int getScore() {
        return score;
    }

    @Override
    public String toString() {
        return name;
    }

    void addWin() {
        score++;
    }
}
