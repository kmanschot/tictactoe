package eu.manschot.tictactoe.domain;

import java.util.List;

public final class GameUtil {
    public static Player getOpponent(List<Player> players, Player player) {
        return players.stream()
                .filter(p -> p.getMark() != player.getMark())
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException(String.format("Player %s not found in list %s", player.getName(), players.toString())));
    }
}
