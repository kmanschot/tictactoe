package eu.manschot.tictactoe.domain;

import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class Position {
    public static final Position X0_Y0 = new Position(0, 0);
    public static final Position X1_Y0 = new Position(1, 0);
    public static final Position X2_Y0 = new Position(2, 0);
    public static final Position X0_Y1 = new Position(0, 1);
    public static final Position X1_Y1 = new Position(1, 1);
    public static final Position X2_Y1 = new Position(2, 1);
    public static final Position X0_Y2 = new Position(0, 2);
    public static final Position X1_Y2 = new Position(1, 2);
    public static final Position X2_Y2 = new Position(2, 2);

    private static final String POSITION_PATTERN = "^([0-2])[:|.]([0-2])$";

    private final int column;
    private final int row;

    public Position(int column, int row) {
        this.column = column;
        this.row = row;
    }

    public int getColumn() {
        return column;
    }

    public int getRow() {
        return row;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Position position = (Position) o;

        return column == position.getColumn() && row == position.getRow();
    }

    @Override
    public int hashCode() {
        return 31 * column + row;
    }

    @Override
    public String toString() {
        return String.format(Locale.getDefault(), "%d:%d", column, row);
    }

    public static Position from(String position) throws IllegalPositionException {
        Pattern pattern = Pattern.compile(POSITION_PATTERN);
        Matcher matcher = pattern.matcher(position);

        if (matcher.find()) {
            String columnResult = matcher.group(1);
            String rowResult = matcher.group(2);

            int column = Integer.parseInt(columnResult);
            int row = Integer.parseInt(rowResult);

            return new Position(column, row);
        }

        throw new IllegalPositionException(position);
    }
}
