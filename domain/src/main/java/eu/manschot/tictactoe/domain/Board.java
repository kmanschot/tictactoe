package eu.manschot.tictactoe.domain;

public class Board {
    public static final int NUMBER_OF_COLUMNS = 3;
    public static final int NUMBER_OF_ROWS = 3;

    private final Field[][] fields;

    Board() {
        fields = new Field[NUMBER_OF_COLUMNS][NUMBER_OF_ROWS];
        for (Field[] column : fields) {
            for (int row=0; row < NUMBER_OF_ROWS; row++) {
                column[row] = new Field();
            }
        }
    }

    public Field[][] getFields() {
        return fields;
    }

    void markField(Mark mark, Position position) throws OccupiedFieldException {
        Field field = fields[position.getColumn()][position.getRow()];

        if (field.hasMark()) {
            throw new OccupiedFieldException(position);
        }

        field.setMark(mark);
    }
}
