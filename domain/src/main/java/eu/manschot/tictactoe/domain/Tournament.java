package eu.manschot.tictactoe.domain;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Tournament {
    private final List<Game> games = new ArrayList<>();
    private final List<Player> players;
    private final CheckWinStrategy winCheckerStrategy;

    private boolean isRunning;
    private Game currentGame;

    private Tournament(CheckWinStrategy winCheckerStrategy) {
        this.winCheckerStrategy = winCheckerStrategy;
        players = Arrays.asList(new Player("1", Mark.CROSS), new Player("2", Mark.NOUGHT));

        startNewGame();
        isRunning = true;
    }

    public List<Player> getPlayers() {
        return players;
    }

    public boolean isRunning() {
        return isRunning;
    }

    public Game getCurrentGame() {
        return currentGame;
    }

    public int getTotalGamesCount() {
        return games.size();
    }

    public static Tournament start(CheckWinStrategy winCheckerStrategy) {
        return new Tournament(winCheckerStrategy);
    }

    public void end() {
        isRunning = false;
        currentGame.end();
    }

    public Game startNewGame() {
        Player playerOnTurn = currentGame != null
                ? GameUtil.getOpponent(players, currentGame.getCurrentPlayer())
                : players.get(0);

        currentGame = Game.start(players, playerOnTurn, winCheckerStrategy);
        games.add(currentGame);

        return currentGame;
    }
}
