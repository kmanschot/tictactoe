package eu.manschot.tictactoe.domain;

public enum Mark {
    CROSS("X"),
    NOUGHT("O");

    private final String presentation;

    Mark(String presentation) {
        this.presentation = presentation;
    }

    public String getPresentation() {
        return presentation;
    }
}
