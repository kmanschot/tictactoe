package eu.manschot.tictactoe.domain;

public interface CheckWinStrategy {
    boolean isWin(Board board, Position position);
}
