package eu.manschot.tictactoe.domain;

public enum GameState {
    DRAW,
    WIN,
    IN_PROGRESS
}
