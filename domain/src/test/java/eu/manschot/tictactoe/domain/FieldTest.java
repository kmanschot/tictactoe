package eu.manschot.tictactoe.domain;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class FieldTest {

    @Test
    public void shouldCreateFieldAsEmpty() {
        Field field = new Field();

        boolean hasMark = field.hasMark();

        assertFalse(hasMark);
    }

    @Test
    public void shouldSetMark() {
        Field field = new Field();
        field.setMark(Mark.CROSS);

        boolean hasMark = field.hasMark();

        assertTrue(hasMark);
    }

    @Test
    public void shouldYieldMark() {
        Field field = new Field();
        field.setMark(Mark.NOUGHT);

        Mark mark = field.getMark();

        assertEquals(Mark.NOUGHT, mark);
    }
}