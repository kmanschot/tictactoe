package eu.manschot.tictactoe.domain;

import org.hamcrest.Matchers;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

class BoardTest {

    @Test
    public void shouldInitialize3x3MatrixOnBoard() {
        Board board = new Board();

        Field[][] fields = board.getFields();

        assertThat(fields.length, Matchers.is(3));

        for (int column = 0; column < 3; column++) {
            assertThat(fields[column].length, Matchers.is(3));
            for (int row = 0; row < 3; row++) {
                assertThat(fields[column][row], Matchers.notNullValue());
            }
        }
    }

    @Test
    public void shouldInitializeBoardWithEmptyFields() {
        Board board = new Board();

        Field[][] fields = board.getFields();

        for (Field[] column : fields) {
            for (Field field : column) {
                assertThat(field.hasMark(), Matchers.is(false));
            }
        }
    }

    @Test
    public void shouldMarkField() throws Exception {
        Board board = new Board();

        board.markField(Mark.CROSS, Position.X0_Y0);

        Field[][] fields = board.getFields();
        for (int column = 0; column < 3; column++) {
            for (int row = 0; row < 3; row++) {
                if (column == 0 && row == 0) {
                    assertThat(fields[column][row].getMark(), equalTo(Mark.CROSS));
                } else {
                    assertThat(fields[column][row].hasMark(), Matchers.is(false));
                }
            }
        }
    }

    @Test
    public void shouldThrowWhenFieldIsOccupied() throws Exception {
        Board board = new Board();

        board.markField(Mark.NOUGHT, Position.X0_Y2);

        Assertions.assertThrows(OccupiedFieldException.class, () -> board.markField(Mark.NOUGHT, Position.X0_Y2));
    }
}