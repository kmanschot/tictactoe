package eu.manschot.tictactoe.domain;

import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;

class OccupiedFieldExceptionTest {

    @Test
    public void shouldContainIllegalValueInMessage() {
        OccupiedFieldException exception = new OccupiedFieldException(Position.X1_Y2);

        String message = exception.getMessage();

        assertThat(message, containsString("1:2"));
    }
}