package eu.manschot.tictactoe.domain;

import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;

import java.util.List;

import static eu.manschot.tictactoe.domain.GameState.DRAW;
import static eu.manschot.tictactoe.domain.GameState.IN_PROGRESS;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.not;

class TournamentTest {
    private CheckWinStrategy checkWinStrategy = new RecursiveCheckWinStrategy();

    @Test
    void shouldProvideCrossMarkToFirstCurrentPlayer() {
        Tournament tournament = Tournament.start(checkWinStrategy);

        Mark mark = tournament.getCurrentGame().getCurrentPlayer().getMark();

        assertThat(mark, equalTo(Mark.CROSS));
    }

    @Test
    void shouldYieldPlayers() {
        Tournament tournament = Tournament.start(checkWinStrategy);

        List<Player> players = tournament.getPlayers();


        assertThat(players.size(), Matchers.is(2));

        Player player1 = players.get(0);
        assertThat(player1.getName(), equalTo("1"));
        assertThat(player1.getMark(), equalTo(Mark.CROSS));

        Player player2 = players.get(1);
        assertThat(player2.getName(), equalTo("2"));
        assertThat(player2.getMark(), equalTo(Mark.NOUGHT));
    }

    @Test
    void shouldBeRunningOnStart() {
        Tournament tournament = Tournament.start(checkWinStrategy);

        boolean isRunning = tournament.isRunning();

        assertThat(isRunning, Matchers.is(true));
    }

    @Test
    void shouldNotBeRunningOnAbort() {
        Tournament tournament = Tournament.start(checkWinStrategy);

        tournament.end();

        assertThat(tournament.isRunning(), Matchers.is(false));
    }

    @Test
    void shouldEndUnfinishedGameInDrawOnTournamentEnd() {
        Tournament tournament = Tournament.start(checkWinStrategy);

        tournament.end();

        assertThat(tournament.getCurrentGame().getState(), Matchers.is(DRAW));
    }

    @Test
    void shouldStartGameOnTournamentStart() {
        Tournament tournament = Tournament.start(checkWinStrategy);

        GameState state = tournament.getCurrentGame().getState();

        assertThat(state, equalTo(IN_PROGRESS));
    }

    @Test
    void shouldYieldGameCount() {
        Tournament tournament = Tournament.start(checkWinStrategy);
        tournament.startNewGame();

        int gamesCount = tournament.getTotalGamesCount();

        assertThat(gamesCount, Matchers.is(2));
    }

    @Test
    void shouldReturnNewGame() {
        Tournament tournament = Tournament.start(checkWinStrategy);
        Game firstGame = tournament.getCurrentGame();
        Game newGame = tournament.startNewGame();

        assertThat(newGame, not(equalTo(firstGame)));
    }

    @Test
    void shouldSwitchFirstPlayerToTurnOnNewGame() {
        Tournament tournament = Tournament.start(checkWinStrategy);

        Player playerOnTurnGame1 = tournament.getCurrentGame().getCurrentPlayer();
        Player playerOnTurnGame2 = tournament.startNewGame().getCurrentPlayer();
        Player playerOnTurnGame3 = tournament.startNewGame().getCurrentPlayer();
        Player playerOnTurnGame4 = tournament.startNewGame().getCurrentPlayer();

        assertThat(playerOnTurnGame1, not(equalTo(playerOnTurnGame2)));
        assertThat(playerOnTurnGame1, equalTo(playerOnTurnGame3));
        assertThat(playerOnTurnGame1, not(equalTo(playerOnTurnGame4)));
        assertThat(playerOnTurnGame2, equalTo(playerOnTurnGame4));
    }
}