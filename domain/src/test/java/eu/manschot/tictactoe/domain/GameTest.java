package eu.manschot.tictactoe.domain;

import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

class GameTest {
    private List<Player> players = Arrays.asList(new Player("1",Mark.CROSS), new Player("2",Mark.NOUGHT));
    private CheckWinStrategy checkWinStrategy = new RecursiveCheckWinStrategy();
    private Player currentPlayer = players.get(0);

    @Test
    void shouldBeInProgressAfterInitialization() {
        Game game = Game.start(players, currentPlayer, checkWinStrategy);

        GameState gameState = game.getState();

        assertThat(gameState, equalTo(GameState.IN_PROGRESS));
    }

    @Test
    void shouldStartWithEmptyBoard() {
        Game game = Game.start(players, currentPlayer, checkWinStrategy);

        Board board = game.getBoard();

        for (Field[] column : board.getFields()) {
            for (Field field : column) {
                assertThat(field.hasMark(), Matchers.is(false));
            }
        }
    }

    @Test
    void shouldMarkField() throws Exception {
        Game game = Game.start(players, currentPlayer, checkWinStrategy);
        Position position = Position.X0_Y1;

        game.markField(position);

        Field field = getField(game.getBoard(), position);
        assertThat(field.hasMark(), Matchers.is(true));
    }

    @Test
    void shouldBeInProgressWhenBoardHasOpenFields() throws Exception {
        Game game = Game.start(players, currentPlayer, checkWinStrategy);
        game.markField(Position.X0_Y0);
        game.markField(Position.X0_Y1);
        game.markField(Position.X0_Y2);
        game.markField(Position.X1_Y0);
        game.markField(Position.X1_Y1);
        game.markField(Position.X1_Y2);
        game.markField(Position.X2_Y1);

        GameState gameState = game.getState();

        assertThat(gameState, equalTo(GameState.IN_PROGRESS));
    }

    @Test
    void shouldDrawGameOnEnd() throws Exception {
        Game game = Game.start(players, currentPlayer, checkWinStrategy);
        game.markField(Position.X0_Y0);
        game.markField(Position.X1_Y0);
        game.markField(Position.X0_Y1);
        game.markField(Position.X1_Y1);
        game.markField(Position.X0_Y2);

        game.end();

        assertThat(game.getState(), equalTo(GameState.WIN));
    }

    @Test
    void shouldDrawGameWhenEndIsCalled() {
        Game game = Game.start(players, currentPlayer, checkWinStrategy);

        game.end();

        assertThat(game.getState(), equalTo(GameState.DRAW));
    }

    @Test
    void shouldBeDrawWhenBoardIsFullWithoutWin() throws Exception {
        Game game = Game.start(players, currentPlayer, checkWinStrategy);
        game.markField(Position.X0_Y0);
        game.markField(Position.X0_Y1);
        game.markField(Position.X0_Y2);
        game.markField(Position.X1_Y0);
        game.markField(Position.X1_Y1);
        game.markField(Position.X2_Y2);
        game.markField(Position.X2_Y1);
        game.markField(Position.X2_Y0);
        game.markField(Position.X1_Y2);

        GameState gameState = game.getState();

        assertThat(gameState, equalTo(GameState.DRAW));
    }

    @Test
    void shouldBeWin() throws Exception {
        Game game = Game.start(players, currentPlayer, checkWinStrategy);
        game.markField(Position.X0_Y0);
        game.markField(Position.X1_Y0);
        game.markField(Position.X0_Y1);
        game.markField(Position.X1_Y1);
        game.markField(Position.X0_Y2);

        GameState gameState = game.getState();

        assertThat(gameState, equalTo(GameState.WIN));
    }

    @Test
    void shouldAddWinToCurrentPlayer() throws Exception {
        Game game = Game.start(players, currentPlayer, checkWinStrategy);
        game.markField(Position.X0_Y0);
        game.markField(Position.X1_Y0);
        game.markField(Position.X0_Y1);
        game.markField(Position.X1_Y1);
        game.markField(Position.X0_Y2);

        assertThat(currentPlayer.getScore(), Matchers.is(1));
        Player opponent = GameUtil.getOpponent(players, currentPlayer);
        assertThat(opponent.getScore(), Matchers.is(0));
    }

    @Test
    void shouldNotTouchOtherFields() throws Exception {
        Game game = Game.start(players, currentPlayer, checkWinStrategy);
        Position position = Position.X0_Y1;

        game.markField(position);

        Field[][] fields = game.getBoard().getFields();
        for (int column = 0; column < Board.NUMBER_OF_COLUMNS; column++) {
            for (int row = 0; row < Board.NUMBER_OF_ROWS; row++) {
                if (column != position.getColumn() && row != position.getRow()) {
                    assertThat(fields[column][row].hasMark(), Matchers.is(false));
                }
            }
        }
    }

    @Test
    void shouldUsePlayerSpecificMarkForEachMove() throws Exception {
        Game game = Game.start(players, currentPlayer, checkWinStrategy);
        Position position1 = Position.X0_Y1;
        Position position2 = Position.X1_Y1;
        Position position3 = Position.X2_Y1;
        Position position4 = Position.X0_Y2;

        game.markField(position1);
        game.markField(position2);
        game.markField(position3);
        game.markField(position4);

        Board board = game.getBoard();
        assertThat(getField(board, position1).getMark(), Matchers.is(Mark.CROSS));
        assertThat(getField(board, position2).getMark(), Matchers.is(Mark.NOUGHT));
        assertThat(getField(board, position3).getMark(), Matchers.is(Mark.CROSS));
        assertThat(getField(board, position4).getMark(), Matchers.is(Mark.NOUGHT));
    }

    @Test
    void shouldYieldCurrentPlayer() throws Exception {
        Game game = Game.start(players, currentPlayer, checkWinStrategy);

        Player prevCurrentPlayer = game.getCurrentPlayer();
        game.markField(Position.X0_Y2);
        Player nextCurrentPlayer = game.getCurrentPlayer();

        assertThat(prevCurrentPlayer.getMark(), equalTo(Mark.CROSS));
        assertThat(nextCurrentPlayer.getMark(), equalTo(Mark.NOUGHT));
    }

    private static Field getField(Board board, Position position) {
        return board.getFields()[position.getColumn()][position.getRow()];
    }
}