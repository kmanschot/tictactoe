package eu.manschot.tictactoe.domain;

import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;

class IllegalPositionExceptionTest {

    @Test
    public void shouldContainIllegalValueInMessage() {
        String illegalPosition = "2:5";
        IllegalPositionException exception = new IllegalPositionException(illegalPosition);

        String message = exception.getMessage();

        assertThat(message, containsString(illegalPosition));
    }

}