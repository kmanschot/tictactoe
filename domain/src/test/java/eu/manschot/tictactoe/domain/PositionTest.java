package eu.manschot.tictactoe.domain;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import nl.jqno.equalsverifier.EqualsVerifier;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

class PositionTest {

    @Test
    void shouldParseUpperLeftPositionFromString() throws IllegalPositionException {
        Position position = Position.from("0:0");

        assertThat(position.getColumn(), equalTo(0));
        assertThat(position.getRow(), equalTo(0));
    }

    @Test
    void shouldParseLowerRightPositionFromString() throws IllegalPositionException {
        Position position = Position.from("2:2");

        assertThat(position.getColumn(), equalTo(2));
        assertThat(position.getRow(), equalTo(2));
    }

    @Test
    void shouldParseFromStringWithDotSeparator() throws IllegalPositionException {
        Position position = Position.from("1.2");

        assertThat(position.getColumn(), equalTo(1));
        assertThat(position.getRow(), equalTo(2));
    }

    @Test
    void shouldParseUpperRightPositionFromString() throws IllegalPositionException {
        Position position = Position.from("0:2");

        assertThat(position.getColumn(), equalTo(0));
        assertThat(position.getRow(), equalTo(2));
    }

    @Test
    void shouldThrowOnIllegalArgument() {
        Assertions.assertThrows(IllegalPositionException.class,() -> Position.from("2:3"));
    }

    @Test
    void shouldYieldToStringForUpperLeftPosition() {
        assertThat(Position.X0_Y0.toString(), equalTo("0:0"));
    }

    @Test
    void shouldYieldToStringForMiddleRightPosition() {
        assertThat(Position.X1_Y2.toString(), equalTo("1:2"));
    }

    @Test
    void shouldAssertEqualsAndHash() {
        EqualsVerifier.forClass(Position.class).verify();
    }
}