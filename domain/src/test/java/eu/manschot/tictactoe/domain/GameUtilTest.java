package eu.manschot.tictactoe.domain;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

class GameUtilTest {
    Player crossPlayer = new Player("1", Mark.CROSS);
    Player noughtPlayer = new Player("2", Mark.NOUGHT);
    List<Player> players = Arrays.asList(crossPlayer, noughtPlayer);

    @Test
    void shouldYieldOpponentOfCrossPlayer() {
        Player opponent = GameUtil.getOpponent(players, crossPlayer);

        assertThat(opponent, equalTo(noughtPlayer));
    }

    @Test
    void shouldYieldOpponentOfNoughtPlayer() {
        Player opponent = GameUtil.getOpponent(players, noughtPlayer);

        assertThat(opponent, equalTo(crossPlayer));
    }
}