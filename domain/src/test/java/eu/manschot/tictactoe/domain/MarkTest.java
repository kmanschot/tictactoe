package eu.manschot.tictactoe.domain;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MarkTest {

    @Test
    public void shouldYieldPresentations() {
        assertEquals("X", Mark.CROSS.getPresentation());
        assertEquals("O", Mark.NOUGHT.getPresentation());
    }
}