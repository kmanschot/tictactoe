package eu.manschot.tictactoe.domain;

import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

class PlayerTest {

    @Test
    void shouldYieldCrossMark() {
        Player player = new Player("1", Mark.CROSS);

        Mark mark = player.getMark();

        assertThat(mark, equalTo(Mark.CROSS));
    }

    @Test
    void shouldYieldNoughtMark() {
        Player player = new Player("2", Mark.NOUGHT);

        Mark mark = player.getMark();

        assertThat(mark, equalTo(Mark.NOUGHT));
    }

    @Test
    void shouldYieldName() {
        String expectedName = "name";
        Player player = new Player(expectedName, Mark.CROSS);

        String name = player.getName();

        assertThat(name, equalTo(expectedName));
    }

    @Test
    void shouldInitializePlayerWithScoreAtZero() {
        Player player = new Player("1", Mark.CROSS);

        int score = player.getScore();

        assertThat(score, Matchers.is(0));
    }

    @Test
    void shouldAddWinToScore() {
        Player player = new Player("1", Mark.CROSS);

        player.addWin();

        assertThat(player.getScore(), Matchers.is(1));
    }

    @Test
    void shouldYieldToString() {
        Player player = new Player("1", Mark.CROSS);

        String actual = player.toString();

        assertThat(actual, Matchers.is("1"));
    }
}