package eu.manschot.tictactoe.domain;

import org.hamcrest.Matchers;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.hamcrest.MatcherAssert.assertThat;

class CheckWinStrategyTest {
    static Stream<Arguments> provideCheckWinStrategies() {
        return Stream.of(
                Arguments.of(new FixedOptionsCheckWinStrategy()),
                Arguments.of(new PartiallyRecursiveCheckWinStrategy()),
                Arguments.of(new RecursiveCheckWinStrategy())
        );
    }

    @ParameterizedTest
    @MethodSource("provideCheckWinStrategies")
    void shouldNotBeWinWhenEmptyBoard(CheckWinStrategy checkWinStrategy) {
        Board board = new Board();

        boolean isWin = checkWinStrategy.isWin(board, Position.X0_Y0);

        assertThat(isWin, Matchers.is(false));
    }

    @ParameterizedTest
    @MethodSource("provideCheckWinStrategies")
    void shouldNotBeWin(CheckWinStrategy checkWinStrategy) throws Exception {
        Board board = new Board();
        board.markField(Mark.CROSS, Position.X0_Y0);
        board.markField(Mark.CROSS, Position.X0_Y1);
        board.markField(Mark.NOUGHT, Position.X0_Y2);

        boolean isWin = checkWinStrategy.isWin(board, Position.X0_Y2);

        assertThat(isWin, Matchers.is(false));
    }

    @ParameterizedTest
    @MethodSource("provideCheckWinStrategies")
    void shouldBeWinByRow0(CheckWinStrategy checkWinStrategy) throws Exception {
        Board board = new Board();
        board.markField(Mark.CROSS, Position.X0_Y0);
        board.markField(Mark.CROSS, Position.X0_Y1);
        board.markField(Mark.CROSS, Position.X0_Y2);

        boolean isWin = checkWinStrategy.isWin(board, Position.X0_Y0);

        assertThat(isWin, Matchers.is(true));
    }

    @ParameterizedTest
    @MethodSource("provideCheckWinStrategies")
    void shouldBeWinByRow1(CheckWinStrategy checkWinStrategy) throws Exception {
        Board board = new Board();
        board.markField(Mark.NOUGHT, Position.X1_Y0);
        board.markField(Mark.NOUGHT, Position.X1_Y1);
        board.markField(Mark.NOUGHT, Position.X1_Y2);

        boolean isWin = checkWinStrategy.isWin(board, Position.X1_Y2);

        assertThat(isWin, Matchers.is(true));
    }

    @ParameterizedTest
    @MethodSource("provideCheckWinStrategies")
    void shouldBeWinByRow2(CheckWinStrategy checkWinStrategy) throws Exception {
        Board board = new Board();
        board.markField(Mark.CROSS, Position.X2_Y0);
        board.markField(Mark.CROSS, Position.X2_Y1);
        board.markField(Mark.CROSS, Position.X2_Y2);

        boolean isWin = checkWinStrategy.isWin(board, Position.X2_Y2);

        assertThat(isWin, Matchers.is(true));
    }

    @ParameterizedTest
    @MethodSource("provideCheckWinStrategies")
    void shouldBeWinByColumn0(CheckWinStrategy checkWinStrategy) throws Exception {
        Board board = new Board();
        board.markField(Mark.NOUGHT, Position.X0_Y0);
        board.markField(Mark.NOUGHT, Position.X1_Y0);
        board.markField(Mark.NOUGHT, Position.X2_Y0);

        boolean isWin = checkWinStrategy.isWin(board, Position.X0_Y0);

        assertThat(isWin, Matchers.is(true));
    }

    @ParameterizedTest
    @MethodSource("provideCheckWinStrategies")
    void shouldBeWinByColumn1(CheckWinStrategy checkWinStrategy) throws Exception {
        Board board = new Board();
        board.markField(Mark.CROSS, Position.X0_Y1);
        board.markField(Mark.CROSS, Position.X1_Y1);
        board.markField(Mark.CROSS, Position.X2_Y1);

        boolean isWin = checkWinStrategy.isWin(board, Position.X0_Y1);

        assertThat(isWin, Matchers.is(true));
    }

    @ParameterizedTest
    @MethodSource("provideCheckWinStrategies")
    void shouldBeWinByColumn2(CheckWinStrategy checkWinStrategy) throws Exception {
        Board board = new Board();
        board.markField(Mark.NOUGHT, Position.X0_Y2);
        board.markField(Mark.NOUGHT, Position.X1_Y2);
        board.markField(Mark.NOUGHT, Position.X2_Y2);

        boolean isWin = checkWinStrategy.isWin(board, Position.X2_Y2);

        assertThat(isWin, Matchers.is(true));
    }

    @ParameterizedTest
    @MethodSource("provideCheckWinStrategies")
    void shouldBeWinByDiagonalBottomTop(CheckWinStrategy checkWinStrategy) throws Exception {
        Board board = new Board();
        board.markField(Mark.CROSS, Position.X0_Y2);
        board.markField(Mark.CROSS, Position.X1_Y1);
        board.markField(Mark.CROSS, Position.X2_Y0);

        boolean isWin = checkWinStrategy.isWin(board, Position.X0_Y2);

        assertThat(isWin, Matchers.is(true));
    }

    @ParameterizedTest
    @MethodSource("provideCheckWinStrategies")
    void shouldBeWinByDiagonalTopBottom(CheckWinStrategy checkWinStrategy) throws Exception {
        Board board = new Board();
        board.markField(Mark.CROSS, Position.X0_Y0);
        board.markField(Mark.CROSS, Position.X1_Y1);
        board.markField(Mark.CROSS, Position.X2_Y2);

        boolean isWin = checkWinStrategy.isWin(board, Position.X2_Y2);

        assertThat(isWin, Matchers.is(true));
    }

    @ParameterizedTest
    @MethodSource("provideCheckWinStrategies")
    void shouldNotBeWin_regressionTestOnDiagonalBugInRecursiveStrategy(CheckWinStrategy checkWinStrategy) throws Exception {
        Board board = new Board();

        board.markField(Mark.CROSS, Position.X0_Y0);
        board.markField(Mark.NOUGHT, Position.X0_Y1);
        board.markField(Mark.CROSS, Position.X0_Y2);
        board.markField(Mark.NOUGHT, Position.X1_Y0);
        board.markField(Mark.CROSS, Position.X1_Y1);
        board.markField(Mark.NOUGHT, Position.X2_Y2);

        boolean isWin = checkWinStrategy.isWin(board, Position.X2_Y2);

        assertThat(isWin, Matchers.is(false));
    }
}
