package eu.manschot.tictactoe.app;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;

import java.util.List;
import java.util.Locale;

import eu.manschot.tictactoe.domain.Board;
import eu.manschot.tictactoe.domain.CheckWinStrategy;
import eu.manschot.tictactoe.domain.Field;
import eu.manschot.tictactoe.domain.Game;
import eu.manschot.tictactoe.domain.OccupiedFieldException;
import eu.manschot.tictactoe.domain.Player;
import eu.manschot.tictactoe.domain.Position;
import eu.manschot.tictactoe.domain.RecursiveCheckWinStrategy;
import eu.manschot.tictactoe.domain.Tournament;

import static eu.manschot.tictactoe.domain.GameState.DRAW;
import static eu.manschot.tictactoe.domain.GameState.IN_PROGRESS;
import static eu.manschot.tictactoe.domain.GameState.WIN;

public class MainActivity extends AppCompatActivity {
    private final BiMap<Position, Integer> positionToButtonIdMap = HashBiMap.create();
    private final CheckWinStrategy checkWinStrategy = new RecursiveCheckWinStrategy();

    private Tournament tournament;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
    }

    @Override
    public void onStart() {
        super.onStart();

        populateBiMap();

        tournament = Tournament.start(checkWinStrategy);

        updateView();
    }

    public void btnNewGame_onClick(View view) {
        if (tournament.isRunning()) {
            tournament.startNewGame();
        } else {
            tournament = Tournament.start(checkWinStrategy);
        }

        updateView();
    }

    public void btnEnd_onClick(View view) {
        tournament.end();

        updateView();
    }

    public void btnExit_onClick(View view) {
        finish();
    }

    public void btnField_onClick(View view) {
        Game game = tournament.getCurrentGame();

        Position position = positionToButtonIdMap.inverse().get(view.getId());
        try {
            game.markField(position);
        } catch (OccupiedFieldException e) {
            e.printStackTrace();
        }

        updateView();
    }

    private void updateView() {
        updateHeader();
        updateGameBoard();
        updateTournamentReport();
        updateMainButtons();
    }

    private void updateMainButtons() {
        boolean gameIsRunning = IN_PROGRESS.equals(tournament.getCurrentGame().getState());
        boolean tournamentIsRunning = tournament.isRunning();

        findViewById(R.id.btn_newGame).setEnabled(!gameIsRunning);
        findViewById(R.id.btn_endTournament).setEnabled(tournamentIsRunning);
        findViewById(R.id.btn_exit).setEnabled(!tournamentIsRunning);
    }

    private void updateTournamentReport() {
        Game game = tournament.getCurrentGame();
        List<Player> players = tournament.getPlayers();
        boolean tournamentIsRunning = tournament.isRunning();

        if (DRAW == game.getState()) {
            setText(R.id.textView_gameReport, "Game ended in a DRAW!");
        } else if (WIN == game.getState()) {
            setText(R.id.textView_gameReport, String.format("Player %s won the game!", game.getCurrentPlayer()));
        } else {
            setText(R.id.textView_gameReport, "");
        }

        TextView textView_tournamentReport = findViewById(R.id.textView_tournamentReport);

        if (tournamentIsRunning) {
            textView_tournamentReport.setText("");
        } else {
            textView_tournamentReport.setText(getTournamentReport(players, tournament.getTotalGamesCount()));
        }
    }

    private void updateGameBoard() {
        Game game = tournament.getCurrentGame();
        Board board = game.getBoard();
        boolean gameIsRunning = IN_PROGRESS == game.getState();

        Field[][] fields = board.getFields();
        positionToButtonIdMap.entrySet().forEach(entrySet -> {
                    Position key = entrySet.getKey();
                    Field field = fields[key.getColumn()][key.getRow()];

                    Button button = findViewById(entrySet.getValue());
                    button.setEnabled(gameIsRunning);

                    if (field.hasMark()) {
                        button.setText(field.getMark().getPresentation());
                    } else {
                        button.setText("");
                    }
                }
        );
    }

    private void updateHeader() {
        updatePlayerSummary(R.id.textView_player1, tournament.getPlayers().get(0));
        updatePlayerSummary(R.id.textView_player2, tournament.getPlayers().get(1));

        setText(R.id.textView_gameRound, String.format("Game round: %s, Player %s to turn", tournament.getTotalGamesCount(), tournament.getCurrentGame().getCurrentPlayer().getName()));
    }

    private void updatePlayerSummary(int textViewId, Player player) {
        setText(textViewId, String.format("Player  %s: %s, Score: %s", player.getName(), player.getMark().getPresentation(), player.getScore()));
    }

    private static String getTournamentReport(List<Player> players, int totalGamesCount) {
        Player player0 = players.get(0);
        Player player1 = players.get(1);

        if (player0.getScore() > player1.getScore()) {
            return formatWinnerReportMessage(player0, player1, totalGamesCount);
        } else if (player0.getScore() < player1.getScore()) {
            return formatWinnerReportMessage(player1, player0, totalGamesCount);
        } else {
            return String.format(Locale.getDefault(), "Tournament ended in a draw after %d game round(s)!", totalGamesCount);
        }
    }

    private static String formatWinnerReportMessage(Player winner, Player loser, int totalGamesCount) {
        String message = "Player %s won the tournament with %d win(s), %d draw(s) and %d loss(es)!";
        int drawCount = totalGamesCount - winner.getScore() - loser.getScore();
        return String.format(Locale.getDefault(), message, winner.getName(), winner.getScore(), drawCount, loser.getScore());
    }

    private void setText(int textViewId, String text) {
        TextView textView_gameRound = findViewById(textViewId);
        textView_gameRound.setText(text);
    }

    private void populateBiMap() {
        positionToButtonIdMap.put(Position.X0_Y0, R.id.button1);
        positionToButtonIdMap.put(Position.X1_Y0, R.id.button2);
        positionToButtonIdMap.put(Position.X2_Y0, R.id.button3);
        positionToButtonIdMap.put(Position.X0_Y1, R.id.button4);
        positionToButtonIdMap.put(Position.X1_Y1, R.id.button5);
        positionToButtonIdMap.put(Position.X2_Y1, R.id.button6);
        positionToButtonIdMap.put(Position.X0_Y2, R.id.button7);
        positionToButtonIdMap.put(Position.X1_Y2, R.id.button8);
        positionToButtonIdMap.put(Position.X2_Y2, R.id.button9);
    }
}
