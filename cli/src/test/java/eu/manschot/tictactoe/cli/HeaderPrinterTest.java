package eu.manschot.tictactoe.cli;

import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import eu.manschot.tictactoe.domain.CheckWinStrategy;
import eu.manschot.tictactoe.domain.Game;
import eu.manschot.tictactoe.domain.Position;
import eu.manschot.tictactoe.domain.RecursiveCheckWinStrategy;
import eu.manschot.tictactoe.domain.Tournament;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;

class HeaderPrinterTest {
    private static int PLAYER_SUMMARY_OFFSET = 4;
    private static int GAME_ROUND_INDEX = 7;

    private ByteArrayOutputStream output = new ByteArrayOutputStream();
    private CheckWinStrategy checkWinStrategy = new RecursiveCheckWinStrategy();

    @Test
    void shouldPrintHeaderGameRoundOne() {
        Tournament tournament = Tournament.start(checkWinStrategy);

        HeaderPrinter printer = new HeaderPrinter(new PrintStream(output));
        printer.print(tournament);

        String outputStr = output.toString();
        assertPlayerSummary(outputStr, 0, "Player 1: X, Score: 0");
        assertPlayerSummary(outputStr, 1, "Player 2: O, Score: 0");
        assertGameRounds(outputStr, "Game round: 1");
    }

    @Test
    void shouldPrintHeaderGameRoundTwo() throws Exception {
        Tournament tournament = Tournament.start(checkWinStrategy);
        Game game = tournament.getCurrentGame();
        game.markField(Position.X0_Y0);
        game.markField(Position.X1_Y0);
        game.markField(Position.X0_Y1);
        game.markField(Position.X1_Y1);
        game.markField(Position.X0_Y2);
        tournament.startNewGame();

        HeaderPrinter printer = new HeaderPrinter(new PrintStream(output));
        printer.print(tournament);

        String outputStr = output.toString();
        assertPlayerSummary(outputStr, 0, "Player 1: X, Score: 1");
        assertPlayerSummary(outputStr, 1, "Player 2: O, Score: 0");
        assertGameRounds(outputStr, "Game round: 2");
    }

    private void assertPlayerSummary(String output, int rowIndex, String expected) {
        String[] lines = output.split("\n");
        String row = lines[PLAYER_SUMMARY_OFFSET + rowIndex];

        assertThat(row, containsString(expected));
    }

    private void assertGameRounds(String output, String expected) {
        String[] lines = output.split("\n");
        String row = lines[GAME_ROUND_INDEX];

        assertThat(row, containsString(expected));

    }
}
