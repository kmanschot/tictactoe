package eu.manschot.tictactoe.cli;

import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;

class AnsiColorTest {

    @Test
    void shouldColorizeStr() {
        String text = "some text";
        String colorizedText = AnsiColor.colorize(text, AnsiColor.WHITE);

        assertThat(colorizedText, Matchers.is(AnsiColor.WHITE.getCode() + text + AnsiColor.RESET.getCode()));
    }

}