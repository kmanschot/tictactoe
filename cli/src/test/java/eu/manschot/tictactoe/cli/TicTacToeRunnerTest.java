package eu.manschot.tictactoe.cli;

import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import eu.manschot.tictactoe.domain.Mark;
import eu.manschot.tictactoe.domain.Position;
import eu.manschot.tictactoe.domain.RecursiveCheckWinStrategy;

import static eu.manschot.tictactoe.cli.GameTestUtil.getMark;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.nullValue;

class TicTacToeRunnerTest {
    private static final String EXIT_GAME = "exit";
    private static final String YES_ON_CONTINUE_TOURNAMENT_QUESTION = "y";
    private static final String NO_ON_CONTINUE_TOURNAMENT_QUESTION = "n";
    private static int BOARD_OFFSET = 11;
    private static int PLAYER_ONE_INDEX = 3;
    private static int PLAYER_TWO_INDEX = 4;
    private static int GAME_ROUND_INDEX = 6;
    private static int GAME_REPORT_INDEX = 20;
    private static int TOURNAMENT_REPORT_INDEX = 22;
    private static int WARNING_INDEX = 22;

    private ByteArrayOutputStream output = new ByteArrayOutputStream();

    @Test
    void shouldStartWithEmptyBoard() {
        List<String> commands = Collections.singletonList(EXIT_GAME);

        createRunner(createInputStream(commands)).run();

        String outputStr = getLastPrintOutput(output.toString());
        assertThat(getMark(outputStr, Position.X0_Y0, BOARD_OFFSET), nullValue());
        assertThat(getMark(outputStr, Position.X1_Y0, BOARD_OFFSET), nullValue());
        assertThat(getMark(outputStr, Position.X2_Y0, BOARD_OFFSET), nullValue());
        assertThat(getMark(outputStr, Position.X0_Y1, BOARD_OFFSET), nullValue());
        assertThat(getMark(outputStr, Position.X1_Y1, BOARD_OFFSET), nullValue());
        assertThat(getMark(outputStr, Position.X2_Y1, BOARD_OFFSET), nullValue());
        assertThat(getMark(outputStr, Position.X0_Y2, BOARD_OFFSET), nullValue());
        assertThat(getMark(outputStr, Position.X1_Y2, BOARD_OFFSET), nullValue());
        assertThat(getMark(outputStr, Position.X2_Y2, BOARD_OFFSET), nullValue());
    }

    @Test
    void shouldPlayGameWin() {
        List<String> commands = Arrays.asList(
                Position.X0_Y0.toString(),
                Position.X1_Y0.toString(),
                Position.X0_Y1.toString(),
                Position.X1_Y1.toString(),
                Position.X0_Y2.toString(),
                NO_ON_CONTINUE_TOURNAMENT_QUESTION
        );

        createRunner(createInputStream(commands)).run();

        String outputStr = getLastPrintOutput(output.toString());
        String[] lines = outputStr.split("\n");

        assertThat(lines[PLAYER_ONE_INDEX], containsString("Player 1: X, Score: 1"));
        assertThat(lines[PLAYER_TWO_INDEX], containsString("Player 2: O, Score: 0"));
        assertThat(lines[GAME_ROUND_INDEX], containsString("Game round: 1"));

        assertThat(getMark(outputStr, Position.X0_Y0, BOARD_OFFSET), Matchers.is(Mark.CROSS));
        assertThat(getMark(outputStr, Position.X1_Y0, BOARD_OFFSET), Matchers.is(Mark.NOUGHT));
        assertThat(getMark(outputStr, Position.X2_Y0, BOARD_OFFSET), nullValue());
        assertThat(getMark(outputStr, Position.X0_Y1, BOARD_OFFSET), Matchers.is(Mark.CROSS));
        assertThat(getMark(outputStr, Position.X1_Y1, BOARD_OFFSET), Matchers.is(Mark.NOUGHT));
        assertThat(getMark(outputStr, Position.X2_Y1, BOARD_OFFSET), nullValue());
        assertThat(getMark(outputStr, Position.X0_Y2, BOARD_OFFSET), Matchers.is(Mark.CROSS));
        assertThat(getMark(outputStr, Position.X1_Y2, BOARD_OFFSET), nullValue());
        assertThat(getMark(outputStr, Position.X2_Y2, BOARD_OFFSET), nullValue());

        assertThat(lines[GAME_REPORT_INDEX], containsString("Player 1 won the game!"));
        assertThat(lines[TOURNAMENT_REPORT_INDEX], containsString("Player 1 won the tournament with 1 win(s), 0 draw(s) and 0 loss(es)!"));
    }

    @Test
    void shouldPlayGameDraw() {
        List<String> commands = new ArrayList<>();
        commands.addAll(getGameMovesForDraw());
        commands.add(NO_ON_CONTINUE_TOURNAMENT_QUESTION);

        createRunner(createInputStream(commands)).run();

        String outputStr = getLastPrintOutput(output.toString());
        String[] lines = outputStr.split("\n");

        assertThat(lines[PLAYER_ONE_INDEX], containsString("Player 1: X, Score: 0"));
        assertThat(lines[PLAYER_TWO_INDEX], containsString("Player 2: O, Score: 0"));
        assertThat(lines[GAME_ROUND_INDEX], containsString("Game round: 1"));

        assertThat(getMark(outputStr, Position.X0_Y0, BOARD_OFFSET), Matchers.is(Mark.CROSS));
        assertThat(getMark(outputStr, Position.X1_Y0, BOARD_OFFSET), Matchers.is(Mark.NOUGHT));
        assertThat(getMark(outputStr, Position.X2_Y0, BOARD_OFFSET), Matchers.is(Mark.NOUGHT));
        assertThat(getMark(outputStr, Position.X0_Y1, BOARD_OFFSET), Matchers.is(Mark.NOUGHT));
        assertThat(getMark(outputStr, Position.X1_Y1, BOARD_OFFSET), Matchers.is(Mark.CROSS));
        assertThat(getMark(outputStr, Position.X2_Y1, BOARD_OFFSET), Matchers.is(Mark.CROSS));
        assertThat(getMark(outputStr, Position.X0_Y2, BOARD_OFFSET), Matchers.is(Mark.CROSS));
        assertThat(getMark(outputStr, Position.X1_Y2, BOARD_OFFSET), Matchers.is(Mark.CROSS));
        assertThat(getMark(outputStr, Position.X2_Y2, BOARD_OFFSET), Matchers.is(Mark.NOUGHT));

        assertThat(lines[GAME_REPORT_INDEX], containsString("Game ended in a DRAW!"));
        assertThat(lines[TOURNAMENT_REPORT_INDEX], containsString("Tournament ended in a draw after 1 game round(s)!"));
    }

    @Test
    void shouldPlayMultipleGameTournament() {
        List<String> commands = new ArrayList<>();
        commands.addAll(getGameMovesForWin());
        commands.add(YES_ON_CONTINUE_TOURNAMENT_QUESTION);
        commands.addAll(getGameMovesForWin());
        commands.add(YES_ON_CONTINUE_TOURNAMENT_QUESTION);
        commands.addAll(getGameMovesForDraw());
        commands.add(YES_ON_CONTINUE_TOURNAMENT_QUESTION);
        commands.addAll(getGameMovesForWin());
        commands.add(NO_ON_CONTINUE_TOURNAMENT_QUESTION);

        createRunner(createInputStream(commands)).run();

        String outputStr = getLastPrintOutput(output.toString());
        String[] lines = outputStr.split("\n");

        assertThat(lines[PLAYER_ONE_INDEX], containsString("Player 1: X, Score: 1"));
        assertThat(lines[PLAYER_TWO_INDEX], containsString("Player 2: O, Score: 2"));
        assertThat(lines[GAME_ROUND_INDEX], containsString("Game round: 4"));

        assertThat(lines[GAME_REPORT_INDEX], containsString("Player 2 won the game!"));
        assertThat(lines[TOURNAMENT_REPORT_INDEX], containsString("Player 2 won the tournament with 2 win(s), 1 draw(s) and 1 loss(es)!"));
    }

    @Test
    void shouldWarnOnIllegalPositionInput() {
        List<String> commands = new ArrayList<>();
        commands.add("3:3");
        commands.add(EXIT_GAME);

        createRunner(createInputStream(commands)).run();

        String outputStr = getLastPrintOutput(output.toString());
        String[] lines = outputStr.split("\n");

        assertThat(lines[WARNING_INDEX], containsString("Warning: the input is not a legal move."));
    }

    @Test
    void shouldWarnOOccupiedFieldInput() {
        List<String> commands = new ArrayList<>();
        commands.add(Position.X0_Y0.toString());
        commands.add(Position.X0_Y0.toString());
        commands.add(EXIT_GAME);

        createRunner(createInputStream(commands)).run();

        String outputStr = getLastPrintOutput(output.toString());
        String[] lines = outputStr.split("\n");

        assertThat(lines[WARNING_INDEX], containsString("Warning: the given field is already occupied."));
    }

    private static String getLastPrintOutput(String outputStr) {
        String[] outputPrints = outputStr.split("TicTacToe");
        return outputPrints[outputPrints.length - 1];
    }

    private InputStream createInputStream(List<String> commands) {
        return new ByteArrayInputStream(String.join("\n", commands).getBytes());
    }

    private TicTacToeRunner createRunner(InputStream inputStream) {
        return new TicTacToeRunner(inputStream, new PrintStream(output), new RecursiveCheckWinStrategy());
    }

    private static List<String> getGameMovesForDraw() {
        return Arrays.asList(
                Position.X0_Y0.toString(),
                Position.X0_Y1.toString(),
                Position.X0_Y2.toString(),
                Position.X1_Y0.toString(),
                Position.X1_Y1.toString(),
                Position.X2_Y2.toString(),
                Position.X2_Y1.toString(),
                Position.X2_Y0.toString(),
                Position.X1_Y2.toString());
    }

    private static List<String> getGameMovesForWin() {
        return Arrays.asList(
                Position.X0_Y0.toString(),
                Position.X1_Y0.toString(),
                Position.X0_Y1.toString(),
                Position.X1_Y1.toString(),
                Position.X0_Y2.toString());
    }
}