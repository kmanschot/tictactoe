package eu.manschot.tictactoe.cli;

import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import eu.manschot.tictactoe.domain.CheckWinStrategy;
import eu.manschot.tictactoe.domain.Game;
import eu.manschot.tictactoe.domain.Mark;
import eu.manschot.tictactoe.domain.Position;
import eu.manschot.tictactoe.domain.RecursiveCheckWinStrategy;
import eu.manschot.tictactoe.domain.Tournament;

import static eu.manschot.tictactoe.cli.GameTestUtil.getMark;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.nullValue;

class GamePrinterTest {
    private static final int GAME_REPORT_INDEX = 12;
    private static int BOARD_OFFSET = 3;

    private ByteArrayOutputStream output = new ByteArrayOutputStream();
    private CheckWinStrategy checkWinStrategy = new RecursiveCheckWinStrategy();

    @Test
    void shouldPrintEmptyBoard() {
        Tournament tournament = Tournament.start(checkWinStrategy);

        new GamePrinter(new PrintStream(output)).print(tournament.getCurrentGame());

        String outputStr = output.toString();
        assertThat(getMark(outputStr, Position.X0_Y0, BOARD_OFFSET), nullValue());
        assertThat(getMark(outputStr, Position.X1_Y0, BOARD_OFFSET), nullValue());
        assertThat(getMark(outputStr, Position.X2_Y0, BOARD_OFFSET), nullValue());
        assertThat(getMark(outputStr, Position.X0_Y1, BOARD_OFFSET), nullValue());
        assertThat(getMark(outputStr, Position.X1_Y1, BOARD_OFFSET), nullValue());
        assertThat(getMark(outputStr, Position.X2_Y1, BOARD_OFFSET), nullValue());
        assertThat(getMark(outputStr, Position.X0_Y2, BOARD_OFFSET), nullValue());
        assertThat(getMark(outputStr, Position.X1_Y2, BOARD_OFFSET), nullValue());
        assertThat(getMark(outputStr, Position.X2_Y2, BOARD_OFFSET), nullValue());
    }

    @Test
    void shouldPrintBoardWithMarks() throws Exception {
        Tournament tournament = Tournament.start(checkWinStrategy);
        Game game = tournament.getCurrentGame();
        game.markField(Position.X0_Y0);
        game.markField(Position.X1_Y2);

        new GamePrinter(new PrintStream(output)).print(game);

        String outputStr = output.toString();
        assertThat(getMark(outputStr, Position.X0_Y0, BOARD_OFFSET), Matchers.is(Mark.CROSS));
        assertThat(getMark(outputStr, Position.X1_Y0, BOARD_OFFSET), nullValue());
        assertThat(getMark(outputStr, Position.X2_Y0, BOARD_OFFSET), nullValue());
        assertThat(getMark(outputStr, Position.X0_Y1, BOARD_OFFSET), nullValue());
        assertThat(getMark(outputStr, Position.X1_Y1, BOARD_OFFSET), nullValue());
        assertThat(getMark(outputStr, Position.X2_Y1, BOARD_OFFSET), nullValue());
        assertThat(getMark(outputStr, Position.X0_Y2, BOARD_OFFSET), nullValue());
        assertThat(getMark(outputStr, Position.X1_Y2, BOARD_OFFSET), Matchers.is(Mark.NOUGHT));
        assertThat(getMark(outputStr, Position.X2_Y2, BOARD_OFFSET), nullValue());
    }

    @Test
    void shouldPrintGameWin() throws Exception {
        Tournament tournament = Tournament.start(checkWinStrategy);
        Game game = tournament.getCurrentGame();
        game.markField(Position.X0_Y0);
        game.markField(Position.X1_Y0);
        game.markField(Position.X0_Y1);
        game.markField(Position.X1_Y1);
        game.markField(Position.X0_Y2);

        new GamePrinter(new PrintStream(output)).print(game);

        String[] lines = output.toString().split("\n");
        assertThat(lines[GAME_REPORT_INDEX], containsString("Player 1 won the game!"));
    }

    @Test
    void shouldPrintGameDraw() throws Exception {
        Tournament tournament = Tournament.start(checkWinStrategy);
        Game game = tournament.getCurrentGame();
        game.markField(Position.X0_Y0);
        game.markField(Position.X0_Y1);
        game.markField(Position.X0_Y2);
        game.markField(Position.X1_Y0);
        game.markField(Position.X1_Y1);
        game.markField(Position.X2_Y2);
        game.markField(Position.X2_Y1);
        game.markField(Position.X2_Y0);
        game.markField(Position.X1_Y2);

        new GamePrinter(new PrintStream(output)).print(game);

        String[] lines = output.toString().split("\n");
        assertThat(lines[GAME_REPORT_INDEX], containsString("Game ended in a DRAW!"));
    }
}