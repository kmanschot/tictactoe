package eu.manschot.tictactoe.cli;

import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import eu.manschot.tictactoe.domain.CheckWinStrategy;
import eu.manschot.tictactoe.domain.Game;
import eu.manschot.tictactoe.domain.Position;
import eu.manschot.tictactoe.domain.RecursiveCheckWinStrategy;
import eu.manschot.tictactoe.domain.Tournament;

import static org.hamcrest.MatcherAssert.assertThat;

class TournamentReportPrinterTest {
    private CheckWinStrategy checkWinStrategy = new RecursiveCheckWinStrategy();
    private ByteArrayOutputStream output = new ByteArrayOutputStream();

    @Test
    void shouldReportDraw() throws Exception {
        Tournament tournament = Tournament.start(checkWinStrategy);
        Game game = tournament.getCurrentGame();
        draw(game);

        new TournamentReportPrinter(new PrintStream(output)).print(tournament);

        assertThat(output.toString(), Matchers.containsString("Tournament ended in a draw after 1 game round(s)!"));
    }



    @Test
    void shouldReportWinForPlayer0() throws Exception {
        Tournament tournament = Tournament.start(checkWinStrategy);
        Game game = tournament.getCurrentGame();
        win(game);
        //this one below is actually a loose due the first user on turn is now player 2. Each game the first user on turn switches.
        win(tournament.startNewGame());
        win(tournament.startNewGame());
        draw(tournament.startNewGame());
        draw(tournament.startNewGame());

        new TournamentReportPrinter(new PrintStream(output)).print(tournament);

        assertThat(output.toString(), Matchers.containsString("Player 1 won the tournament with 2 win(s), 2 draw(s) and 1 loss(es)!"));
    }

    @Test
    void shouldReportWinForPlayer1() throws Exception {
        Tournament tournament = Tournament.start(checkWinStrategy);
        Game game = tournament.getCurrentGame();
        draw(game);
        win(tournament.startNewGame());
        draw(tournament.startNewGame());

        new TournamentReportPrinter(new PrintStream(output)).print(tournament);

        assertThat(output.toString(), Matchers.containsString("Player 2 won the tournament with 1 win(s), 2 draw(s) and 0 loss(es)!"));
    }

    private void draw(Game game) throws Exception {
        game.markField(Position.X0_Y0);
        game.markField(Position.X0_Y1);
        game.markField(Position.X0_Y2);
        game.markField(Position.X1_Y0);
        game.markField(Position.X1_Y1);
        game.markField(Position.X2_Y2);
        game.markField(Position.X2_Y1);
        game.markField(Position.X2_Y0);
        game.markField(Position.X1_Y2);
    }

    private void win(Game game) throws Exception {
        game.markField(Position.X0_Y0);
        game.markField(Position.X1_Y0);
        game.markField(Position.X0_Y1);
        game.markField(Position.X1_Y1);
        game.markField(Position.X0_Y2);
    }
}