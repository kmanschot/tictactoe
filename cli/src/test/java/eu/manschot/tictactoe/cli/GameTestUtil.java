package eu.manschot.tictactoe.cli;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import eu.manschot.tictactoe.domain.Mark;
import eu.manschot.tictactoe.domain.Position;

class GameTestUtil {
    private static final String ROW_PATTERN = ".*([.XO]).*([.XO]).*([.XO])";

    static Mark getMark(String printedOutput, Position position, int rowOffset) {
        String[] lines = printedOutput.split("\n");
        int rowIndex = rowOffset + 2 * (position.getRow());
        String row = String.valueOf(lines[rowIndex]);

        Pattern rowPattern = Pattern.compile(ROW_PATTERN);
        Matcher matcher = rowPattern.matcher(row);

        if (matcher.find()) {
            String field = matcher.group(position.getColumn() + 1);
            if (Mark.CROSS.getPresentation().equals(field)) {
                return Mark.CROSS;
            }
            if (Mark.NOUGHT.getPresentation().equals(field)) {
                return Mark.NOUGHT;
            } else {
                return null;
            }
        }

        throw new RuntimeException("Field not found");
    }
}
