package eu.manschot.tictactoe.cli;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;

import eu.manschot.tictactoe.domain.CheckWinStrategy;
import eu.manschot.tictactoe.domain.Game;
import eu.manschot.tictactoe.domain.IllegalPositionException;
import eu.manschot.tictactoe.domain.OccupiedFieldException;
import eu.manschot.tictactoe.domain.Position;
import eu.manschot.tictactoe.domain.Tournament;

import static eu.manschot.tictactoe.cli.AnsiColor.colorize;
import static eu.manschot.tictactoe.domain.GameState.IN_PROGRESS;

class TicTacToeRunner {
    private static final Logger LOGGER = LoggerFactory.getLogger(TicTacToeRunner.class);

    private final PrintStream printStream;
    private final BufferedReader inputReader;
    private final CheckWinStrategy checkWinStrategy;
    private final HeaderPrinter headerPrinter;
    private final GamePrinter gamePrinter;
    private final TournamentReportPrinter tournamentReportPrinter;

    private String errorMessage = null;

    TicTacToeRunner(InputStream inputStream, PrintStream printStream, CheckWinStrategy checkWinStrategy) {
        this.printStream = printStream;
        this.checkWinStrategy = checkWinStrategy;

        headerPrinter = new HeaderPrinter(printStream);
        gamePrinter = new GamePrinter(printStream);
        tournamentReportPrinter = new TournamentReportPrinter(printStream);

        inputReader = new BufferedReader(new InputStreamReader(inputStream));
    }

    void run() {
        Tournament tournament = Tournament.start(checkWinStrategy);

        while (tournament.isRunning()) {

            headerPrinter.print(tournament);
            gamePrinter.print(tournament.getCurrentGame());
            printErrorMessage();

            Game currentGame = tournament.getCurrentGame();

            if (currentGame.getState() == IN_PROGRESS) {
                errorMessage = null;

                String input = requestUserInput(String.format("Player %s, please enter field as x:y (eg. 1:2):", currentGame.getCurrentPlayer().getName()), tournament::end);

                if (isExit(input)) {
                    tournament.end();
                    continue;
                }

                Position newMove = null;

                try {
                    newMove = Position.from(input);
                } catch (IllegalPositionException e) {
                    LOGGER.warn("Illegal position is given on user move request", e);

                    errorMessage = "the input is not a legal move.";
                }

                if (newMove != null) {
                    try {
                        currentGame.markField(newMove);
                    } catch (OccupiedFieldException e) {
                        LOGGER.warn("Illegal position is given on user move request", e);
                        errorMessage = "the given field is already occupied.";
                    }
                }
            } else {
                String input = requestUserInput("Continue to play? (y/n)", tournament::end);

                if ("y".equalsIgnoreCase(input)) {
                    tournament.startNewGame();
                } else if ("n".equalsIgnoreCase(input)) {
                    tournament.end();
                }
            }
        }

        tournamentReportPrinter.print(tournament);
    }

    private void printErrorMessage() {
        if (errorMessage != null) {
            printStream.println(colorize("Warning: " + errorMessage, AnsiColor.RED));
        }
    }

    private static boolean isExit(String move) {
        return "q".equalsIgnoreCase(move) || "quit".equalsIgnoreCase(move) || "exit".equalsIgnoreCase(move);
    }

    private String requestUserInput(String requestMessage, Runnable exceptionHandler) {
        printStream.println(colorize(requestMessage, AnsiColor.WHITE));

        String userInput = "";
        try {
            userInput = inputReader.readLine();

        } catch (IOException e) {
            LOGGER.error("Exception occurred on requesting user move", e);
            closeQuietly(inputReader);

            exceptionHandler.run();
        }

        return userInput;
    }

    private void closeQuietly(Closeable resource) {
        try {
            if (resource != null) {
                resource.close();
            }
        } catch (Exception e) {
            LOGGER.error("Thrown exception on resource.close()", e);
        }
    }
}
