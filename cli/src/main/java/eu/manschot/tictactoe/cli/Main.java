package eu.manschot.tictactoe.cli;

import eu.manschot.tictactoe.domain.CheckWinStrategy;
import eu.manschot.tictactoe.domain.RecursiveCheckWinStrategy;

public class Main {
    public static void main(String[] args) {
        //CheckWinStrategy fixedOptionsStrategy = new FixedOptionsCheckWinStrategy();
        //CheckWinStrategy partiallyRecursiveStrategy = new PartiallyRecursiveCheckWinStrategy();
        CheckWinStrategy recursiveStrategy = new RecursiveCheckWinStrategy();

        new TicTacToeRunner(System.in, System.out, recursiveStrategy).run();
    }
}
