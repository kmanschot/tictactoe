package eu.manschot.tictactoe.cli;

import java.io.PrintStream;
import java.util.List;
import java.util.Locale;

import eu.manschot.tictactoe.domain.Player;
import eu.manschot.tictactoe.domain.Tournament;

import static eu.manschot.tictactoe.cli.AnsiColor.colorize;

class TournamentReportPrinter {
    private final PrintStream printStream;

    TournamentReportPrinter(PrintStream printStream) {
        this.printStream = printStream;
    }

    void print(Tournament tournament) {
        List<Player> players = tournament.getPlayers();

        Player player0 = players.get(0);
        Player player1 = players.get(1);
        int totalGamesCount = tournament.getTotalGamesCount();

        String report;

        if (player0.getScore() > player1.getScore()) {
            report =  formatWinnerReportMessage(player0, player1, totalGamesCount);
        } else if (player0.getScore() < player1.getScore()) {
            report = formatWinnerReportMessage(player1, player0, totalGamesCount);
        } else {
            report = String.format(Locale.getDefault(), "Tournament ended in a draw after %d game round(s)!", totalGamesCount);
        }

        printStream.println(colorize(report, AnsiColor.WHITE));
    }

    private String formatWinnerReportMessage(Player winner, Player loser, int totalGamesCount) {
        String message = "Player %s won the tournament with %d win(s), %d draw(s) and %d loss(es)!";
        int drawCount = totalGamesCount - winner.getScore() - loser.getScore();
        return String.format(Locale.getDefault(), message, winner.getName(), winner.getScore(), drawCount, loser.getScore());
    }
}
