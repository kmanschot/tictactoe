package eu.manschot.tictactoe.cli;

public enum AnsiColor {
    RESET("\u001B[0m"),
    RED("\u001B[31m"),
    BLUE("\u001B[34m"),
    GREEN("\u001B[32m"),
    MAGENTA("\u001B[35m"),
    WHITE("\u001B[37m"),
    CYAN("\u001B[36m");

    private final String code;

    AnsiColor(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public static String colorize(String str, AnsiColor color) {
        return String.format("%s%s%s", color.getCode(), str, AnsiColor.RESET.getCode());
    }
}
