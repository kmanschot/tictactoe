package eu.manschot.tictactoe.cli;

import java.io.PrintStream;

import eu.manschot.tictactoe.domain.Tournament;

class HeaderPrinter {
    private static final String CLEAR_CONSOLE_CODE = "\033[H\033[2J";

    private final PrintStream printStream;

    HeaderPrinter(PrintStream printStream) {
        this.printStream = printStream;
    }

    void print(Tournament tournament) {
        StringBuilder builder = new StringBuilder();

        builder.append(CLEAR_CONSOLE_CODE);

        builder.append("\n");
        builder.append("     TicTacToe    ");
        builder.append("\n");
        builder.append("      *  *  *     ");
        builder.append("\n");
        builder.append("\n");

        tournament.getPlayers().forEach(player -> {
                    builder.append(String.format("Player %s: %s, Score: %s", player.getName(), player.getMark().getPresentation(), player.getScore()));
                    builder.append("\n");
                }
        );

        builder.append("\n");
        builder.append(String.format("Game round: %s", tournament.getTotalGamesCount()));
        builder.append("\n");
        builder.append("\n");

        printStream.print(builder.toString());
    }
}
