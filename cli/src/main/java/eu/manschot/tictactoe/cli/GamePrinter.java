package eu.manschot.tictactoe.cli;

import java.io.PrintStream;

import eu.manschot.tictactoe.domain.Board;
import eu.manschot.tictactoe.domain.Field;
import eu.manschot.tictactoe.domain.Game;
import eu.manschot.tictactoe.domain.Mark;

import static eu.manschot.tictactoe.cli.AnsiColor.colorize;
import static eu.manschot.tictactoe.domain.GameState.DRAW;
import static eu.manschot.tictactoe.domain.GameState.IN_PROGRESS;
import static eu.manschot.tictactoe.domain.GameState.WIN;

class GamePrinter {
    private static final String EMPTY_FIELD = ".";

    private final PrintStream printStream;

    GamePrinter(PrintStream printStream) {
        this.printStream = printStream;
    }

    void print(Game game) {
        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append("    0    1    2");
        stringBuilder.append("\n");
        stringBuilder.append("  ---------------");
        stringBuilder.append("\n");
        stringBuilder.append(" |               | ");
        stringBuilder.append("\n");


        Field[][] fields = game.getBoard().getFields();

        for (int column = 0; column < Board.NUMBER_OF_COLUMNS; column++) {
            for (int row = 0; row < Board.NUMBER_OF_ROWS; row++) {
                final Field field = fields[row][column];

                if (row == 0) {
                    stringBuilder.append(String.format("%s|", column));
                }

                stringBuilder.append(String.format("  %s  ", getPresentation(field)));

                if (row == 2) {
                    stringBuilder.append(String.format("|%s", column));
                    stringBuilder.append("\n");
                    stringBuilder.append(" |               | ");
                    stringBuilder.append("\n");
                }
            }
        }

        stringBuilder.append("  ---------------");
        stringBuilder.append("\n");
        stringBuilder.append("    0    1    2");
        stringBuilder.append("\n");
        stringBuilder.append("\n");

        if (game.getState() == IN_PROGRESS) {
            printTipsToQuit(stringBuilder);
        } else if (game.getState() == DRAW) {
            stringBuilder.append(colorize("Game ended in a DRAW!", AnsiColor.WHITE));
            stringBuilder.append("\n");
        } else if (game.getState() == WIN) {
            stringBuilder.append(colorize(String.format("Player %s won the game!", game.getCurrentPlayer()), AnsiColor.WHITE));
            stringBuilder.append("\n");
        }

        printStream.print(stringBuilder.toString());
    }

    private void printTipsToQuit(StringBuilder stringBuilder) {
        stringBuilder.append("To quit, type: (q)uit, exit.");
        stringBuilder.append("\n");
        stringBuilder.append("\n");
    }

    private String getPresentation(Field field) {
        if (field.hasMark()) {
            Mark mark = field.getMark();

            return String.format("\u001B[1m%s%s%s", getANSIColor(mark).getCode(), mark.getPresentation(), AnsiColor.RESET.getCode());
        }

        return EMPTY_FIELD;
    }

    private AnsiColor getANSIColor(Mark mark) {
        return mark == Mark.CROSS ? AnsiColor.GREEN : AnsiColor.BLUE;
    }
}
